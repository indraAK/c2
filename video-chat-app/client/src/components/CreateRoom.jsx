const Room = (props) => {
  const create = async (e) => {
    e.preventDefault();

    const resp = await fetch("http://localhost:8000/create");
    const { room_id } = await resp.json();

    props.history.push(`/room/${room_id}`);
  };
  return (
    <div className="min-h-screen grid place-items-center">
      <button
        onClick={create}
        className="inline-flex justify-center items-center px-6 py-3 bg-purple-700 text-white font-medium tracking-wide rounded-full shadow hover:bg-purple-600 focus:bg-purple-600"
      >
        Create Room
      </button>
    </div>
  );
};

export default Room;
