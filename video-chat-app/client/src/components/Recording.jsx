import { useReactMediaRecorder } from "react-media-recorder";
import { createFileName } from "use-react-screenshot";
import moment from "moment";

const Recording = () => {
  const vidName = moment().format("DD-MM-YYYY hh:mm:ss") + " - Rec";
  const { startRecording, stopRecording, mediaBlobUrl } = useReactMediaRecorder(
    { screen: true }
  );
  const download = (video, { name = `${vidName}`, extension = "mp4" } = {}) => {
    const a = document.createElement("a");
    a.href = mediaBlobUrl;
    a.download = createFileName(extension, name);
    a.click();
  };

  return (
    <div className="mt-3 flex items-center gap-2">
      <button
        onClick={startRecording}
        className="inline-flex justify-center items-center align-middle px-4 py-2 bg-teal-700 text-white font-medium tracking-wide rounded shadow hover:bg-teal-600 focus:bg-teal-600 active:scale-95 transition duration-200 ease-out"
      >
        Record
      </button>
      <button
        onClick={stopRecording}
        className="inline-flex justify-center items-center align-middle px-4 py-2 bg-red-500 text-white font-medium tracking-wide rounded shadow hover:bg-red-400 focus:bg-red-400 active:scale-95 transition duration-200 ease-out"
      >
        Stop Record
      </button>
      {mediaBlobUrl && (
        <button
          onClick={download}
          className="inline-flex justify-center items-center align-middle px-4 py-2 bg-purple-700 text-white font-medium tracking-wide rounded shadow hover:bg-purple-600 focus:bg-purple-600 active:scale-95 transition duration-200 ease-out"
        >
          Download
        </button>
      )}
    </div>
  );
};
export default Recording;
