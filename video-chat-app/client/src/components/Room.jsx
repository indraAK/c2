import { useEffect, useRef, createRef, useState } from "react";
import { useScreenshot, createFileName } from "use-react-screenshot";
import moment from "moment";
import Recording from "./Recording";

const Room = (props) => {
  const ref = createRef(null);
  const userVideo = useRef();
  const userStream = useRef();
  const partnerVideo = useRef();
  const peerRef = useRef();
  const webSocketRef = useRef();

  // screen capture
  const [image, takeScreenShot] = useScreenshot({
    type: "image/png",
    quality: 1.0,
  });

  const ssName = moment().format("DD-MM-YYYY hh:mm:ss") + " - Screenshot";

  const download = (image, { name = `${ssName}`, extension = "png" } = {}) => {
    const a = document.createElement("a");
    a.href = image;
    a.download = createFileName(extension, name);
    a.click();
  };

  const downloadScreenshot = () => takeScreenShot(ref.current).then(download);

  const openCamera = async () => {
    const allDevices = await navigator.mediaDevices.enumerateDevices();
    const cameras = allDevices.filter((device) => device.kind == "videoinput");
    console.log(cameras);

    const constraints = {
      audio: true,
      video: {
        deviceId: cameras.deviceId,
      },
    };

    try {
      return await navigator.mediaDevices.getUserMedia(constraints);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    openCamera().then((stream) => {
      userVideo.current.srcObject = stream;
      userStream.current = stream;

      webSocketRef.current = new WebSocket(
        `ws://127.0.0.1:8000/join?roomID=${props.match.params.roomID}`
      );
      webSocketRef.current.addEventListener("open", () => {
        webSocketRef.current.send(JSON.stringify({ join: true }));
      });
      webSocketRef.current.addEventListener("message", async (e) => {
        const message = JSON.parse(e.data);

        if (message.join) {
          callUser();
        }
        if (message.offer) {
          handleOffer(message.offer);
        }
        if (message.answer) {
          console.log("Receiving Answer");
          peerRef.current.setRemoteDescription(
            new RTCSessionDescription(message.answer)
          );
        }
        if (message.iceCandidate) {
          console.log("Receiving and Adding ICE Candidate");
          try {
            await peerRef.current.addIceCandidate(message.iceCandidate);
          } catch (error) {
            console.log("Error Receiving ICE Candidate", error);
          }
        }
      });
    });
  }, []);
  const handleOffer = async (offer) => {
    console.log("Received Offer Creating Answer");
    peerRef.current = createPeer();

    await peerRef.current.setRemoteDescription(
      new RTCSessionDescription(offer)
    );
    userStream.current.getTracks().forEach((track) => {
      peerRef.current.addTrack(track, userStream.current);
    });
    const answer = await peerRef.current.createAnswer();
    await peerRef.current.setLocalDescription(answer);
    webSocketRef.current.send(
      JSON.stringify({ answer: peerRef.current.localDescription })
    );
  };

  const callUser = () => {
    console.log("Calling Other User");
    peerRef.current = createPeer();

    userStream.current.getTracks().forEach((track) => {
      peerRef.current.addTrack(track, userStream.current);
    });
  };

  const createPeer = () => {
    console.log("Creating Peer Connection");
    const peer = new RTCPeerConnection({
      iceServers: [{ urls: "stun:stun.l.google.com:19302" }],
    });
    peer.onnegotiationneeded = handleNegotiationNeeded;
    peer.onicecandidate = handleIceCandidateEvent;
    peer.ontrack = handleTrackEvent;

    return peer;
  };

  const handleNegotiationNeeded = async () => {
    console.log("Creating Offer");

    try {
      const myOffer = await peerRef.current.createOffer();
      await peerRef.current.setLocalDescription(myOffer);

      webSocketRef.current.send(
        JSON.stringify({ offer: peerRef.current.localDescription })
      );
    } catch (error) {
      console.log("Negotiation Error", error);
    }
  };

  const handleIceCandidateEvent = (e) => {
    console.log("Found Ice Candidate");
    if (e.candidate) {
      console.log(e.candidate);
      webSocketRef.current.send(JSON.stringify({ iceCandidate: e.candidate }));
    }
  };

  const handleTrackEvent = (e) => {
    console.log("Received Tracks");
    partnerVideo.current.srcObject = e.streams[0];
  };

  return (
    <div ref={ref} className="max-w-4xl mx-auto p-6">
      <div className="grid gap-4 grid-cols-1 md:grid-cols-2">
        <video
          autoPlay={true}
          controls={true}
          ref={userVideo}
          className="w-full"
        ></video>
        <video
          autoPlay={true}
          controls={true}
          ref={partnerVideo}
          className="w-full"
        ></video>
      </div>
      <div className="mt-6">
        <button
          className="inline-flex justify-center items-center align-middle px-4 py-2 bg-slate-900 text-white font-medium tracking-wide rounded shadow hover:bg-slate-800 focus:bg-slate-800 active:scale-95 transition duration-200 ease-out"
          style={{ marginBottom: "10px" }}
          onClick={downloadScreenshot}
        >
          Screen Capture
        </button>
        <Recording />
      </div>
    </div>
  );
};
export default Room;
